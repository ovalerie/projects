import requests, os, random, string, sys
from dotenv import load_dotenv
from urllib.error import HTTPError

## i am just making an edit in browser

load_dotenv()
key = os.environ["KEY"]
secret = os.environ["SECRET"]
auth = os.environ["AUTH"]

headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": f"Basic {auth}",
}

info = {
    "event_id": 121230,
    "email": "mytestemail@swoogo.com",
    "first_name": "John",
    "last_name": "Doe",
    "reviewer": 1,
    "rating": 4,
    "date": "2024-12-12",
    "notes": "The cat could very well be mans best friend but would never stoop to admitting it.",
    "submission_name": "Ethical AI And The Future Of Semi Skilled Labor.",
    "start_time": "03:00:00",
    "end_time": "04:00:00",
    "company": "Swoogo",
    "job_title": "QA Tester",
}
payload = {
    "reviewer": 1,
    "submitter": 1,
    "name": "The Effects Of Magnesium for Sleep.",
    "start_time": "08:00:00",
    "end_time": "09:00:00",
    "notes": "Before you marry a person, first make them use a computer with slow internet to see who they really are.",
    "rating": 5,
}

session = requests.Session()

# generates the token needed for authentication
def get_token():
    try:
        r = requests.post(
            "https://www.swoogo.com/api/v1/oauth2/token.json",
            headers=headers,
            timeout=5,
            data={"grant_type": "client_credentials"},
        )
        r.raise_for_status()
        response = r.json()
        token = response["access_token"]
        return token
    except HTTPError as error:
        print(f"Error: {error.reason}")
        raise SystemExit(error)
    
token = get_token()

session.headers.update({"Content-Type": "application/x-www-form-urlencoded","Authorization": f"Bearer {token}"})

# tests all endpoints
def test_all():
    global payload
    urls = [
        "https://api.swoogo.com/api/v1/submissions",
        "https://api.swoogo.com/api/v1/cfs-contacts",
        "https://api.swoogo.com/api/v1/cfs-reviews",
    ]
    ids = []
    try:
        submission = session.post(
            "https://api.swoogo.com/api/v1/submissions/create.json",
            # headers=headers,
            data={
                "event_id": info["event_id"],
                "name": info["submission_name"],
                "date": info["date"],
                "start_time": info["start_time"],
                "end_time": info["end_time"],
            },
            timeout=5,
        )
        submission.raise_for_status()
        submission = submission.json()
        submission_id = submission["id"]
        ids.append(submission_id)
    except HTTPError as error:
        print(f"Error: {error.reason}")
        raise SystemExit(error)
    print("Submission created.")
    try:
        reviewer = session.post(
            "https://api.swoogo.com/api/v1/cfs-contacts/create.json",
            data={
                "event_id": info["event_id"],
                "email": info["email"],
                "first_name": info["first_name"],
                "last_name": info["last_name"],
                "company": info["company"],
                "job_title": info["job_title"],
            },
            timeout=5,
        )
        reviewer.raise_for_status()
        reviewer = reviewer.json()
        reviewer_id = reviewer["id"]
        ids.append(reviewer_id)
    except HTTPError as error:
        print(f"Error: {error.reason}")
        raise SystemExit(error)
    print("Contact created.")
    try:
        review = session.post(
            "https://api.swoogo.com/api/v1/cfs-reviews/create.json",
            data={
                "event_id": info["event_id"],
                "submission_id": submission_id,
                "reviewer_id": reviewer_id,
                "rating": info["rating"],
                "notes": info["notes"],
            },
            timeout=5,
        )
        review.raise_for_status()
        review = review.json()
        review_id = review["id"]
        ids.append(review_id)
    except HTTPError as error:
        print(f"Error: {error.reason}")
        raise SystemExit(error)
    print("Review created.")
    # test the get all endpoints
    for url in urls:
        try:
            r = session.get(
                f"{url}.json", params={"event_id": 121230}, timeout=5
            )
            if r.status_code != 200:
                sys.exit(
                    "Something didn't work as expected and the get ALL call failed"
                )
        except HTTPError as error:
            print(f"Error: {error.reason}")
            raise SystemExit(error)
    print("Tested get all endpoints successfully.")

    # tests the updates endpoints
    for i in range(3):
        try:
            r = session.put(
                f"{urls[i]}/update/{ids[i]}.json",
                timeout=5,
                data=payload,
            )
            if "cfs-contacts" in r.request.path_url:
                del payload["reviewer"]
                del payload["submitter"]
            elif "submissions" in r.request.path_url:
                del payload["name"]
                del payload["start_time"]
                del payload["end_time"]
            elif "cfs-reviews" in r.request.path_url:
                continue
            else:
                print(f"something went wrong here {r.request.path_url}")
        except HTTPError as error:
            print(f"Error: {error.reason}")
            raise SystemExit(error)
    print("Tested update endpoints successfully.")

    # tests the get a single item endpoints
    for i in range(3):
        try:
            r = session.get(f"{urls[i]}/{ids[i]}.json", timeout=5)
            res = r.json()
            if "cfs-contacts" in r.request.path_url:
                if res["submitter"] == True and res["reviewer"] == True:
                    continue
            elif "submissions" in r.request.path_url:
                if res["name"] == "The Effects Of Magnesium for Sleep.":
                    continue
            elif "cfs-reviews" in r.request.path_url:
                if res["rating"] == 5 and len(res["notes"]) == 32:
                    continue
            else:
                print(f"looks like something went wrong here {r.request.path_url}")
        except HTTPError as error:
            print(f"Error: {error.reason}")
            raise SystemExit(error)
    print("Tested all 3 get a single item endpoints successfully.")

    # tests the delete endpoints
    for i in reversed(range(3)):
        try:
            r = session.delete(
                f"{urls[i]}/delete/{ids[i]}.json",
                timeout=5,
                data=payload,
            )
            if r.status_code == 204:
                continue
        except HTTPError as error:
            print(f"Error: {error.reason}")
            raise SystemExit(error)
    print("Tested all delete endpoints successfully.")


# generates 3 different type of cfs contacts based on their roles
def generate_cfs_contacts(num_of_addresses):
    first_names = [
        "Bruis",
        "Niki",
        "Merci",
        "Ronny",
        "Hallsy",
        "Roderich",
        "Georgie",
        "Janeen",
        "Conan",
        "Angelia",
        "Lulu",
        "Jermain",
        "Valentijn",
        "Aile",
        "Petronille",
        "Marian",
        "Renaldo",
        "Nerissa",
        "Roshelle",
        "Brier",
        "Katha",
        "Estel",
        "Helge",
        "Willard",
        "Hannis",
        "Nickola",
        "Lark",
        "Kial",
        "Jammie",
        "Angy",
        "Elliott",
        "Rosemary",
        "Marlowe",
        "Bryna",
        "Maybelle",
        "Marius",
        "Rosco",
        "Marjorie",
        "Bianka",
        "Bambi",
        "Oswald",
        "Berny",
        "Sandor",
        "Claribel",
        "Nancy",
        "Anatollo",
        "Woodrow",
        "Kippar",
        "Donella",
        "Lenee",
        "Danie",
        "Shirlee",
        "Gaspar",
        "Lucila",
        "Ruth",
        "Dilly",
        "Cheslie",
        "Sherwin",
        "Tiphani",
        "Paten",
    ]
    email_list = []
    last_names = [
        "Kynaston",
        "Emps",
        "Arni",
        "Dengel",
        "Pottle",
        "Shoute",
        "Bucksey",
        "Demange",
        "Allicock",
        "Pomery",
        "Beddon",
        "Collman",
        "Leeb",
        "Eykelbosch",
        "Lagadu",
        "Wrotham",
        "Tawton",
        "Sisnett",
        "Novotna",
        "Bente",
        "Dumbrill",
        "Wegman",
        "Bartholomaus",
        "Crombie",
        "Fawthorpe",
        "Benedikt",
        "Mertsching",
        "Enochsson",
        "Marvel",
        "Hilbourne",
        "Lippard",
        "Bramley",
        "Bilfoot",
        "Kinghorne",
        "Cadwallader",
        "Storer",
        "Follett",
        "Degli Antoni",
        "Tuberfield",
        "Germain",
        "Pinke",
        "D'Aubney",
        "Catonne",
        "Delicate",
        "Ezzy",
        "Beldon",
        "Galea",
        "Hampshaw",
        "Keuning",
        "Fawkes",
        "Philippe",
        "Yosifov",
        "Maylor",
        "Dingley",
        "Campey",
        "McPeeters",
        "Heninghem",
        "Serris",
        "Whotton",
        "Seydlitz",
    ]

    reviewers = []
    submitters = []
    bothfum = []

    # generates unique email addresses and adds them to a list
    for _ in range(num_of_addresses):
        username = "".join(random.choices(string.ascii_lowercase, k=7))
        domain = "@swoogo.com"
        email = f"{username}{domain}"
        email_list.append(email)
    # generates 25 submitter objects
    for i in range(25):
        submitter = {
            "first_name": first_names[i],
            "last_name": last_names[i],
            "email": email_list[i],
            "event_id": 121230,
            "submitter": 1,
            "reviewer": 0,
            "company": "Swoogo",
            "job_title": "Tester",
        }
        submitters.append(submitter)
    # generates 25 reviewer objects
    for i in range(25, 50):
        reviewer = {
            "first_name": first_names[i],
            "last_name": last_names[i],
            "email": email_list[i],
            "event_id": 121230,
            "submitter": 0,
            "reviewer": 1,
            "company": "Swoogo",
            "job_title": "Tester",
        }
        reviewers.append(reviewer)
    # generates 10 objects that are both reviewers and submitters
    for i in range(50, 60):
        both = {
            "first_name": first_names[i],
            "last_name": last_names[i],
            "email": email_list[i],
            "event_id": 121230,
            "submitter": 1,
            "reviewer": 1,
            "company": "Swoogo",
            "job_title": "Tester",
        }
        bothfum.append(both)

    return submitters, reviewers, bothfum


# creates contacts
def create_contacts():
    submitters, reviewers, both = generate_cfs_contacts(60)
    reviewers_ids = []

    for sub in submitters:
        try:
            payload = {
                "event_id": f"{sub['event_id']}",
                "first_name": f"{sub['first_name']}",
                "last_name": f"{sub['last_name']}",
                "email": f"{sub['email']}",
                "reviewer": f"{sub['reviewer']}",
                "submitter": f"{sub['submitter']}",
                "company": f"{sub['company']}",
                "job_title": f"{sub['job_title']}",
            }
            r = session.post(
                "https://api.swoogo.com/api/v1/cfs-contacts/create.json",
                data=payload,
                timeout=5,
            )
            r.raise_for_status()
            print("working in the background")
        except HTTPError as error:
            print(f"Error: {error.reason}")
            raise SystemExit(error)
    print("Created 25 submitters.")

    for rew in reviewers:
        try:
            payload = {
                "event_id": f"{rew['event_id']}",
                "first_name": f"{rew['first_name']}",
                "last_name": f"{rew['last_name']}",
                "email": f"{rew['email']}",
                "reviewer": f"{rew['reviewer']}",
                "submitter": f"{rew['submitter']}",
                "company": f"{rew['company']}",
                "job_title": f"{rew['job_title']}",
            }
            r = session.post(
                "https://api.swoogo.com/api/v1/cfs-contacts/create.json",
                data=payload,
                timeout=5,
            )
            r.raise_for_status()
            response = r.json()
            reviewer_id = response["id"]
            reviewers_ids.append(reviewer_id)
            print("working in the background")
        except HTTPError as error:
            print(f"Error: {error.reason}")
                        
            raise SystemExit(error)

    print("Created 25 revievwers.")

    for b in both:
        try:
            payload = {
                "event_id": f"{b['event_id']}",
                "first_name": f"{b['first_name']}",
                "last_name": f"{b['last_name']}",
                "email": f"{b['email']}",
                "reviewer": f"{b['reviewer']}",
                "submitter": f"{b['submitter']}",
                "company": f"{b['company']}",
                "job_title": f"{b['job_title']}",
            }
            r = session.post(
                "https://api.swoogo.com/api/v1/cfs-contacts/create.json",
                data=payload,
                timeout=5,
            )
            r.raise_for_status()
            print("working in the background")
        except HTTPError as error:
            print(f"Error: {error.reason}")
            raise SystemExit(error)
    print("Created 10 contacts who are reviewers and submitters.")

    return reviewers_ids


# creates submissions
def create_submissions():
    submissions = []
    names = [
        "Virtual reciprocal capability",
        "Object-based intermediate algorithm",
        "Organized fresh-thinking customer loyalty",
        "Automated secondary secured line",
        "Advanced attitude-oriented hardware",
        "Robust solution-oriented interface",
        "Organic full-range array",
        "Phased actuating utilisation",
        "Vision-oriented client-driven definition",
        "Re-engineered mobile model",
        "Open-source executive flexibility",
        "Expanded eco-centric groupware",
        "Down-sized leading edge initiative",
        "Assimilated zero administration challenge",
        "Reverse-engineered grid-enabled concept",
        "Multi-lateral impactful hub",
        "Re-contextualized optimal monitoring",
        "Customizable dedicated function",
        "Seamless scalable alliance",
        "Operative leading edge info-mediaries",
    ]
    description = "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ratione quos, vel quia."
    submissions_ids = []

    for i in range(20):
        submission = {
            "event_id": 121230,
            "name": names[i],
            "description": description,
            "date": "2024-12-12",
            "start_time": "03:00:00",
            "end_time": "06:00:00",
            "location_id": 372905,
            "track_id": 101509,
            "session_status": "submitted",
        }
        submissions.append(submission)

    for submission in submissions:
        try:
            payload = {
                "event_id": f"{submission['event_id']}",
                "name": f"{submission['name']}",
                "description": f"{submission['description']}",
                "date": f"{submission['date']}",
                "start_time": f"{submission['start_time']}",
                "end_time": f"{submission['end_time']}",
                "location_id": f"{submission['location_id']}",
                "track_id": f"{submission['track_id']}",
                "session_status": f"{submission['session_status']}",
            }
            r = session.post(
                "https://api.swoogo.com/api/v1/submissions/create.json",
                data=payload,
                timeout=5,
            )
            r.raise_for_status()
            response = r.json()
            submission_id = response["id"]
            submissions_ids.append(submission_id)
            print("working in the background")
        except HTTPError as error:
            print(f"Error: {error.reason}")
            raise SystemExit(error)
    print("Created 20 submissions.")

    return submissions_ids


# creates reviews
def create_reviews():
    submissions = create_submissions()
    reviewers = create_contacts()
    try:
        for sub in submissions:
            print("Creating reviews for each submission.")
            random.shuffle(reviewers)
            for i in range(3):
                rating = random.randint(1, 5)
                payload = {
                    "event_id": 121230,
                    "submission_id": sub,
                    "reviewer_id": reviewers[i],
                    "rating": rating,
                    "notes": "A random note",
                }
                r = session.post(
                    "https://api.swoogo.com/api/v1/cfs-reviews/create.json",
                    data=payload,
                    timeout=5,
                )
                r.raise_for_status()
    except HTTPError as error:
        print(f"Error: {error.reason}")
        raise SystemExit(error)
    print("Created 3 reviews for each submssion")
    print("Testing each endpoint Done.")


test_all()
print("Moving on to creating bulk contacts, submissions and reviews.")
create_reviews()
