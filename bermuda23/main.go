package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strings"
)


func main() {
	filePath := os.Args[1]
	file, err := os.Open(filePath)
	fileWords := []string{}

	if err != nil {
		fmt.Print(err)
	}

	defer file.Close()

	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanWords)
	
	for fileScanner.Scan() {
		word := fileScanner.Text()
		word = regexp.MustCompile(`[^a-zA-Z0-9 ]+`).ReplaceAllString(word, "")
		word = strings.ToLower(word)
		re := regexp.MustCompile(".{3}")
		wordn := re.FindAllString(word, -1)
		fileWords = append(fileWords, wordn...)
	}

	wordsMap := make(map[string]int)

	for _, v := range fileWords {
		wordsMap[v] = wordsMap[v] +1
	}
	
	var pairs []struct {
		Key string
		Value int
	}

	for k, v := range wordsMap {
		pairs = append(pairs, struct{Key string; Value int}{k,v})
	}

	sort.Slice(pairs, func(i, j int) bool {
		return pairs[i].Value > pairs[j].Value
	})

	topTwenty := pairs[:20]

	fmt.Println(topTwenty)

}