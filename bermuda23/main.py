import re, sys, fileinput, string, re
from operator import itemgetter
from is_ascii import check_if_assci
from collections import Counter


def check_if_assci(file):
    none_asc_word = [] #holds words with none ascii characters
    is_asc_word = [] #hold words with all ascii characters
    
    with fileinput.input(file) as f:
        line = f.readline()
        
        if not line:
            print("It looks like the file is empty")
            
        while line:
            line = f.readline()
            line = line.translate(str.maketrans("", "", string.punctuation)) #remove punctuations
            line = re.sub(r"[^\w\s]", "", line)
            
            lines = line.split()
            for word in lines:
                if word.isascii() and len(word) >= 3:
                    is_asc_word.append(word)
                else:
                    none_asc_word.append(word)
                
    return is_asc_word

def divide_into_three():
    broken_words = []
    file = sys.argv[1]
    
    all_ascii_words = check_if_assci(file) #returns a list containing only ascii words
    
    for word in all_ascii_words:
        word = word.lower()
        hold_words = re.findall("...", word) #breaks word into 3s, and returns the list
        broken_words += hold_words #append the content of hold_word list
        
    preview = Counter(broken_words) #returns the count of each occurence in broken words and returns a dict with the counts as value and the words as keys
    preview = dict(sorted(preview.items(), key=itemgetter(1), reverse=True)) #sort the dict by values
    top_twenty = list(preview.items())[:20] #convert dictionary to list and return the first 20
    
    print(top_twenty)
    
    return top_twenty
divide_into_three()