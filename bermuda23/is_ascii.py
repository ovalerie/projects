import fileinput, string, re


def check_if_assci(file):
    none_asc_word = []
    is_asc_word = []
    with fileinput.input(file) as f:
        line = f.readline()
        
        if not line:
            print("It looks like the file is empty")
            
        while line:
            line = f.readline()
            line = line.translate(str.maketrans("", "", string.punctuation)) #remove punctuations
            line = re.sub(r"[^\w\s]", "", line)
            
            lines = line.split()
            for word in lines:
                if word.isascii() and len(word) >= 3:
                    is_asc_word.append(word)
                else:
                    none_asc_word.append(word)
                
    return is_asc_word